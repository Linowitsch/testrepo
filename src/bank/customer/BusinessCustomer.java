package bank.customer;

import java.util.LinkedList;
import java.util.List;

import bank.products.Account;
import bank.products.Credit;


public class BusinessCustomer {

	private String name;
	private String forename;
	private Date birthdate;
	
	private List<Account> accounts;
	private List<Credit> credits;
	

	public BusinessCustomer(String name) {		
		this.name = name;
		
		accounts = new LinkedList<Account>();
		credits = new LinkedList<Credit>();
	}

	
	
	public void setName(String name) {
		if(this.name == "Otto")
			this.name = "Otto Waalkes";
		this.name = name;
	}
	
	public void setForename(String forename){
		this.forename = forename;
	}

	public void setBirthdate(Date birthdate){
		if(birthdate>new Date(2014,08,22))
			println("Das geht nicht");
		if(birthdate<new Date(1900,01,01))
			println("auch sehr unwahrscheinlich")
		this.birthdate = birthdate;
	}
	
	public String getName() {
		return name;
	}



	public List<Account> getAccounts() {
		return accounts;
	}



	public List<Credit> getCredits() {
		return credits;
	}
	
	

}
